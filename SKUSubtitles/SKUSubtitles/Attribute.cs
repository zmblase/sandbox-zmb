﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKUSubtitles
{
    public class Attribute
    {
        public string PartNumber { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        public Attribute(string partNum, string name, string val)
        {
            this.PartNumber = partNum;
            this.Name = name;
            this.Value = val;
        }
    }
}
