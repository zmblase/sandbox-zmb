﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using array = global::System.Array;
using gS = global::System;


namespace SKUSubtitles
{
    public class Program
    {
        static void Main(string[] args)
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Modern Truck" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h10, h11, h18, h17 };
            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);
            VehicleSubtitle(h1313);


            Attribute att1 = new Attribute("1", "Diameter", "3/4'");
            Attribute att2 = new Attribute("2", "lbs", "10");
            Attribute att3 = new Attribute("3", "Color", "Orange");
            Attribute att4 = new Attribute("4", "Height", "1'");
            //Attribute att5 = new Attribute("5", "Quantity", "10");
            //Attribute att6 = new Attribute("8", "Part #", "4857");
            //Attribute att7 = new Attribute("6", "Origin", "United States");
            var Attributes = new List<Attribute> { att1, att2, att3, att4, };
            Universal univ = new Universal(Attributes);
            UniversalSubtitle(univ);

            var engFam = new List<EngineFamily>();
            List<string> tranFam = new List<string>(new string[] { "Henchcraft", "Stanley", "AFCO", "Rear", "Front" });
            List<string> axleFam = new List<string>(new string[] { });
            Driveline driveline = new Driveline(engFam, tranFam, axleFam);
            DrivelineSubtitle(driveline);

            Console.ReadLine();

        }

        public static string DrivelineSubtitle (Driveline driveline)
        {
            var sortEngineFamily = driveline.EngineFamily.OrderBy(x => x.EngineFam).ToList();
            driveline.TransFamily.Sort();
            driveline.AxleFamily.Sort();
            string subtitle = "";
            int tempCount;
            StringBuilder builder = new StringBuilder();
            
            for (int i = 0; i < sortEngineFamily.Count; i++)
            {
                Console.WriteLine(sortEngineFamily[i].EngineFam);
            }
            if (driveline.EngineFamily.Count > 0)
            {
                for (int i = 0; i < driveline.EngineFamily.Count; i++)
                {
                    builder.AppendFormat("{0} ", sortEngineFamily[i].EngineFam);
                    for (int j = 0; j < sortEngineFamily[i].EngineSubFam.Count; j++)
                    {
                        builder.AppendFormat("{0} ", sortEngineFamily[i].EngineSubFam[j]);
                    }
                    builder.Length--;
                    builder.Append(", ");
                }
                if (builder.Length + subtitle.Length - 2 <= 65)
                {
                    subtitle += builder;
                }
                //If Option A is longer than 65 characters for Engine Family
                else
                {
                    builder.Clear();
                    for (int i = 0; i < sortEngineFamily.Count; i++)
                    {
                        
                        builder.AppendFormat("{0}, ", sortEngineFamily[i].EngineFam);
                        
                    }
                    Console.WriteLine(builder);
                    Console.WriteLine(subtitle.Length + builder.Length);
                    if (builder.Length + subtitle.Length - 2 <= 65)
                    {
                        subtitle += builder;
                    }
                    //If Option B is longer than 65 characters for Engine Family
                    else
                    {
                        if (subtitle.Length > 0)
                        {
                            subtitle += ", ";
                        }
                        builder.Clear();
                        tempCount = subtitle.Length;
                        for (int i = 0; builder.Length + tempCount - 2 <= 65; i++)
                        {
                            builder.AppendFormat("{0}, ", sortEngineFamily[i].EngineFam);
                            if (builder.Length + tempCount - 2 <= 65)
                            {
                                subtitle += sortEngineFamily[i].EngineFam + ", ";
                            }
                        }
                    }
                }
                subtitle = subtitle.Substring(0, subtitle.Length - 2);
            }

            if (driveline.TransFamily.Count > 0)
            {
                builder.Clear();
                for (int i = 0; i < driveline.TransFamily.Count; i++)
                {
                    builder.AppendFormat("{0}, ", driveline.TransFamily[i]);
                }
                if (builder.Length + subtitle.Length - 2 <= 65)
                {
                    subtitle += builder;
                }
                //If Option A is longer than 54 hcaracters for Transmission Family
                else
                {
                    if (subtitle.Length > 0)
                    {
                        subtitle += ", ";
                    }
                    builder.Clear();
                    tempCount = subtitle.Length;
                    for (int j = 0; builder.Length + tempCount - 2 <= 65; j++)
                    {
                        builder.AppendFormat("{0}, ", driveline.TransFamily[j]);
                        if (builder.Length + tempCount - 2 <= 65)
                        {
                            subtitle += driveline.TransFamily[j] + ", ";
                        }
                    }
                }
                
                subtitle = subtitle.Substring(0, subtitle.Length - 2);
            }
            if (driveline.AxleFamily.Count > 0)
            {
                builder.Clear();
                for (int i = 0; i < driveline.AxleFamily.Count; i++)
                {
                    builder.AppendFormat("{0}, ", driveline.AxleFamily[i]);
                }
                if (builder.Length + subtitle.Length - 2 <= 65)
                {
                    subtitle += builder;
                }
                //If Option A is longer than 54 hcaracters for Transmission Family
                else
                {
                    if (subtitle.Length > 0)
                    {
                        subtitle += ", ";
                    }
                    builder.Clear();
                    tempCount = subtitle.Length;
                    for (int j = 0; builder.Length + tempCount - 2 <= 65; j++)
                    {
                        builder.AppendFormat("{0}, ", driveline.AxleFamily[j]);
                        if (builder.Length + tempCount - 2 <= 65)
                        {
                            subtitle += driveline.AxleFamily[j] + ", ";
                        }
                    }
                }

                subtitle = subtitle.Substring(0, subtitle.Length - 2);
            }
            //Console.WriteLine(subtitle.Length);
            //Console.WriteLine(subtitle);
            return subtitle;
        }
        public static string UniversalSubtitle (Universal univ)
        {

            var att =
                from y in univ.AttList
                orderby y.PartNumber 
                select y;

            List<string> Names = att.Select(x => x.Name).ToList();
            List<string> Values = att.Select(x => x.Value).ToList();

            string subtitle = "";
            int tempCount;
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < univ.AttList.Count; i++)
            {
                if (Names[i].Contains("Color") || Names[i] == "Finish" || Names[i] == "Material Type")
                {
                    builder.AppendFormat("{0}, ", Values[i]);
                }
                else
                {
                    builder.AppendFormat("{0} {1}, ", Values[i], Names[i]);
                }
            }
            if (subtitle.Length + builder.Length - 2 <= 65)
            {
                subtitle += builder;
            }
            else
            {
                builder.Clear();
                tempCount = subtitle.Length;
                for (int j = 0; j < Values.Count; j++)
                {
                    if (Names[j].Contains("Color") || Names[j] == "Finish" || Names[j] == "Material Type")
                    {
                        builder.Clear();
                        builder.AppendFormat("{0}, ", Values[j]);
                        if (subtitle.Length + builder.Length - 2 <= 65)
                        {
                            subtitle += Values[j] + ", ";
                        }
                    }
                    else
                    {
                        builder.Clear();
                        builder.AppendFormat("{0} {1}, ", Values[j], Names[j]);
                        if (subtitle.Length + builder.Length - 2 <= 65)
                        {
                            subtitle += Values[j] + " " + Names[j] + ", ";
                        }
                    }
                }
            }
            subtitle = subtitle.Substring(0, subtitle.Length - 2);

            Console.WriteLine(subtitle.Length);
            Console.WriteLine(subtitle);
            return subtitle;
        }

        public static string VehicleSubtitle (SKU sku)
        {
            var modelQuery =
                from y in sku.YMM
                orderby y.Model
                group y by y.Model into m
                select new { Model = m.Key };

            var makeQuery =
                from y in sku.YMM
                orderby y.Model
                group y by y.Make into m
                select new { Make = m.Key };

            List<string> models = modelQuery.Select(x => x.Model).ToList();
            List<string> makesByModel = new List<string>(new string[models.Count]);
            List<string> makesByModelAlpha = new List<string>(new string[models.Count]);
            List<string> modelByMakeAlpha = new List<string>(new string[makesByModelAlpha.Count]);
            List<string> makesByYear = new List<string>(new string[sku.YMM.Count]);
            List<string> makesByMake = makeQuery.Select(x => x.Make).ToList();
            makesByMake.Sort();
            models.Sort();
            //Populates the list "makesByModel" with an individial make for every model in alphabetical order of the list "models"
            for (int i = 0; i < models.Count; i++)
            {
                for (int j = 0; j < sku.YMM.Count; j++)
                {
                    if (sku.YMM[j].Model == models[i])
                    {
                        makesByModel[i] = sku.YMM[j].Make;
                    }
                }
            }

            //Populates the list "makesByModelAlpa" with an individual make for every model in alphabetical order 
            makesByModelAlpha = makesByModel;
            makesByModelAlpha.Sort();

            //Populate the list "modelByMakeAlpha" with an individual model for every make in alphabetical order

            for (int i = 0; i < makesByModelAlpha.Count; i++)
            {
                for (int j = 0; j < sku.YMM.Count; j++)
                {
                    if (sku.YMM[j].Make == makesByModelAlpha[i])
                    {
                        modelByMakeAlpha[i] = sku.YMM[j].Model;
                    }
                }
            }

            //Creates list of lists where sublists of years are indexed according to the model list
            List < List<int> > yearsByModel = new List<List<int>>();
            for (int i = 0; i < models.Count; i++)
            {
                yearsByModel.Add(new List<int>());
            }
            for (int i = 0; i < models.Count; i++)
            {
                for (int j = 0; j < sku.YMM.Count; j++)
                {
                    if (sku.YMM[j].Model == models[i])
                    {
                        yearsByModel[i].Add(sku.YMM[j].Year);
                    }
                }
            }
            for (int i = 0; i < models.Count; i++)
            {
                yearsByModel[i].Sort();
            }

            //Creates list of lists where sublists of years are indexed according to the make list
            List<List<int>> yearsByMake = new List<List<int>>();
            for (int i = 0; i < makesByMake.Count; i++)
            {
                yearsByMake.Add(new List<int>());
            }
            for (int i = 0; i < makesByMake.Count; i++)
            {
                for (int j = 0; j < sku.YMM.Count; j++)
                {
                    if (sku.YMM[j].Make == makesByMake[i])
                    {
                        yearsByMake[i].Add(sku.YMM[j].Year);
                    }
                }
            }
            for (int i = 0; i < makesByMake.Count; i++)
            {
                yearsByMake[i].Sort();
            }



            //Creates alphabetical list of Pedal Car Applications
            List<string> pedalApps = new List<string>();
            pedalApps.AddRange(sku.PedalApps);
            pedalApps.Sort();

            //Creates alphabetical list of Race Types
            List<string> raceTypes = new List<string>();
            raceTypes.AddRange(sku.RaceTypes);
            raceTypes.Sort();

            string subtitle = "";
            StringBuilder builder = new StringBuilder();
            int tempCount = 0;

            if (sku.Markets.Contains("Pedal Car"))
            {
                builder.Clear();
                for (int i = 0; i < pedalApps.Count; i++)
                {
                    builder.AppendFormat("{0}, ", pedalApps[i]);
                }
                if (builder.Length + subtitle.Length - 2 <= 65)
                {
                    subtitle += builder;
                    subtitle = subtitle.Substring(0, subtitle.Length - 2);
                }
                //If Option A is longer than 65
                else
                {
                    builder.Clear();
                    tempCount = subtitle.Length;
                    for (int i = 0; builder.Length + tempCount - 2 <= 65; i++)
                    {
                        builder.AppendFormat("{0}, ", pedalApps[i]);
                        Console.WriteLine(builder);
                        Console.WriteLine(builder.Length + subtitle.Length);
                        if (builder.Length - 2 <= 65)
                        {
                            subtitle += pedalApps[i] + ", ";
                        }
                    }
                    subtitle = subtitle.Substring(0, subtitle.Length - 2);
                }
            }

            if (sku.Markets.Contains("Open Wheel") || sku.Markets.Contains("Oval Track"))
            {
                builder.Clear();
                for (int i = 0; i < raceTypes.Count; i++)
                {
                    builder.AppendFormat("{0}, ", raceTypes[i]);
                }
                if (builder.Length + subtitle.Length - 2 <= 65)
                {
                    subtitle += builder;
                    subtitle = subtitle.Substring(0, subtitle.Length - 2);
                }
                //If Option A is longer than 65
                else
                {
                    builder.Clear();
                    tempCount = subtitle.Length;
                    for (int i = 0; builder.Length + tempCount - 2 <= 65; i++)
                    {
                        builder.AppendFormat("{0}, ", raceTypes[i]);
                        if (builder.Length - 2 <= 65)
                        {
                            subtitle += raceTypes[i] + ", ";
                        }
                    }
                    subtitle = subtitle.Substring(0, subtitle.Length - 2);
                }
            }

            if (sku.Markets.Contains("T-Bucket"))
            {
                builder.Clear();
                for (int i = 0; i < models.Count; i++)
                {
                    int[] yearArray = yearsByModel[i].ToArray();
                    int[][] result = ConsecutiveSequences(yearArray);

                    foreach (var item in result)
                    {
                        if (item[0] != item[item.Length - 1])
                        {
                            builder.Append(item[0].ToString().Substring(2, item[0].ToString().Length - 2) + "-" + item[item.Length - 1].ToString().Substring(2, item[item.Length - 1].ToString().Length - 2) + " " + models[i] + ", ");
                        }
                        else if (item[0] == item[item.Length - 1])
                        {
                            builder.Append(item[item.Length - 1].ToString().Substring(2, item[item.Length - 1].ToString().Length - 2) + " " + models[i] + ", ");
                        }
                    }
                }
                if (builder.Length + subtitle.Length - 2 <= 65)
                {
                    if (subtitle.Length > 0)
                    {
                        subtitle += ", ";
                    }
                    subtitle += builder;
                    subtitle = subtitle.Substring(0, subtitle.Length - 2);
                }
                //If Option A is longer than 65
                else
                {
                    builder.Clear();
                    for (int i = 0; i < models.Count; i++)
                    {
                        builder.AppendFormat("{0}, ", models[i]);
                    }

                    if (builder.Length + subtitle.Length - 2 <= 65)
                    {
                        if (subtitle.Length > 0)
                        {
                            subtitle += ", ";
                        }
                        subtitle += builder;
                        subtitle = subtitle.Substring(0, subtitle.Length - 2);
                    }
                    //If Option B is longer than 65
                    else
                    {
                        if (subtitle.Length > 0)
                        {
                            subtitle += ", ";
                        }
                        builder.Clear();
                        tempCount = subtitle.Length;
                        for (int i = 0; builder.Length + tempCount - 2 <= 65; i++)
                        {
                            builder.AppendFormat("{0}, ", models[i]);
                            if (builder.Length + tempCount - 2 <= 65)
                            {
                                subtitle += models[i] + ", ";
                            }
                        }
                        subtitle = subtitle.Substring(0, subtitle.Length - 2);
                    }
                }
                builder.Clear();
            }
            if (sku.Markets.Contains("Modern Truck") || sku.Markets.Contains("Muscle Car") || sku.Markets.Contains("Sport Compact") || sku.Markets.Contains("Classic Truck") || sku.Markets.Contains("Modern Muscle") || sku.Markets.Contains("Street Rod"))
            {
                builder.Clear();
                for (int i = 0; i < makesByMake.Count; i++)
                {
                    int[] yearArray = yearsByMake[i].ToArray();
                    int[][] result = ConsecutiveSequences(yearArray);

                    foreach (var item in result)
                    {
                        if (item[0] != item[item.Length - 1])
                        {
                            builder.Append(item[0].ToString().Substring(2, item[0].ToString().Length - 2) + "-" + item[item.Length - 1].ToString().Substring(2, item[item.Length - 1].ToString().Length - 2) + " " + makesByModelAlpha[i] + " " + modelByMakeAlpha[i] + ", ");
                        }
                        else if (item[0] == item[item.Length - 1])
                        {
                            builder.Append(item[item.Length - 1].ToString().Substring(2, item[item.Length - 1].ToString().Length - 2) + " " + makesByModelAlpha[i] + " " + modelByMakeAlpha[i] + ", ");
                        }
                    }
                }
                if (builder.Length + subtitle.Length - 2 <= 65)
                {
                    if (subtitle.Length > 0)
                    {
                        subtitle += ", ";
                    }
                    subtitle += builder;
                    subtitle = subtitle.Substring(0, subtitle.Length - 2);
                }
                //If Option A is longer than 65 
                else
                {
                    builder.Clear();
                    if (!sku.Markets.Contains("Modern Truck") && !sku.Markets.Contains("Muscle Car") && !sku.Markets.Contains("Sport Compact"))
                    {
                        builder.Clear();
                        if (sku.Markets.Contains("Modern Muscle"))
                        {
                            for (int i = 0; i < models.Count; i++)
                            {
                                int[] years = yearsByModel[i].ToArray();
                                int[][] result = ConsecutiveSequences(years);

                                foreach (var item in result)
                                {
                                    if (item[0] != item[item.Length - 1])
                                    {
                                        builder.Append(item[0].ToString().Substring(2, item[0].ToString().Length - 2) + "-" + item[item.Length - 1].ToString().Substring(2, item[item.Length - 1].ToString().Length - 2) + " " + models[i] + ", ");
                                    }
                                    else if (item[0] == item[item.Length - 1])
                                    {
                                        builder.Append(item[item.Length - 1].ToString().Substring(2, item[item.Length - 1].ToString().Length - 2) + " " + models[i] + ", ");
                                    }
                                }
                            }

                            if (builder.Length + subtitle.Length - 2 <= 65)
                            {
                                if (subtitle.Length > 0)
                                {
                                    subtitle += ", ";
                                }
                                subtitle += builder;
                                subtitle = subtitle.Substring(0, subtitle.Length - 2);
                            }
                            //If Option B is longer than 65 for Modern Muscle
                            else
                            {
                                builder.Clear();
                                for (int i = 0; i < models.Count; i++)
                                {
                                    
                                    builder.AppendFormat("{0}, ", models[i]);
                                }

                                if (builder.Length + subtitle.Length - 2 <= 65)
                                {
                                    if (subtitle.Length > 0)
                                    {
                                        subtitle += ", ";
                                    }
                                    subtitle += builder;
                                    subtitle = subtitle.Substring(0, subtitle.Length - 2);
                                }
                                //If Option C is longer than 65 for Modern Muscle
                                else
                                {
                                    builder.Clear();
                                    for (int i = 0; i < makesByMake.Count; i++)
                                    {
                                        builder.AppendFormat("{0}, ", makesByMake[i]);
                                    }

                                    if (builder.Length + subtitle.Length - 2 <= 65)
                                    {
                                        if (subtitle.Length > 0)
                                        {
                                            subtitle += ", ";
                                        }
                                        subtitle += builder;
                                        subtitle = subtitle.Substring(0, subtitle.Length - 2);
                                    }
                                    //If Option D is longer than 65 for Modern Muscle
                                    else
                                    {
                                        if (subtitle.Length > 0)
                                        {
                                            subtitle += ", ";
                                        }
                                        builder.Clear();
                                        tempCount = subtitle.Length;
                                        for (int i = 0; builder.Length + tempCount - 2 <= 65; i++)
                                        {
                                            builder.AppendFormat("{0}, ", makesByMake[i]);
                                            if (builder.Length + tempCount - 2 <= 65)
                                            {
                                                subtitle += makesByMake[i] + ", ";
                                            }
                                        }
                                        subtitle = subtitle.Substring(0, subtitle.Length - 2);
                                    }
                                }
                            }
                        }
                        else if (sku.Markets.Contains("Classic Truck") || sku.Markets.Contains("Street Rod"))
                        {
                            builder.Clear();
                            for (int i = 0; i < makesByMake.Count; i++)
                            {
                                int[] years = yearsByMake[i].ToArray();
                                int[][] result = ConsecutiveSequences(years);

                                foreach (var item in result)
                                {
                                    if (item[0] != item[item.Length - 1])
                                    {
                                        builder.Append(item[0].ToString().Substring(2, item[0].ToString().Length - 2) + "-" + item[item.Length - 1].ToString().Substring(2, item[item.Length - 1].ToString().Length - 2) + " " + makesByMake[i] + ", ");
                                    }
                                    else if (item[0] == item[item.Length - 1])
                                    {
                                        builder.Append(item[item.Length - 1].ToString().Substring(2, item[item.Length - 1].ToString().Length - 2) + " " + makesByMake[i] + ", ");
                                    }
                                }
                            }

                            if (builder.Length + subtitle.Length - 2 <= 65)
                            {
                                if (subtitle.Length > 0)
                                {
                                    subtitle += ", ";
                                }
                                subtitle += builder;
                                subtitle = subtitle.Substring(0, subtitle.Length - 2);
                            }
                            //If Option B is longer than 65 for Classic Truck or Street Rod
                            else
                            {
                                builder.Clear();
                                for (int i = 0; i < models.Count; i++)
                                {
                                    builder.AppendFormat("{0} ", makesByModelAlpha[i]);
                                    builder.AppendFormat("{0}, ", modelByMakeAlpha[i]);
                                }
                                if (builder.Length + subtitle.Length - 2 <= 65)
                                {
                                    if (subtitle.Length > 0)
                                    {
                                        subtitle += ", ";
                                    }
                                    subtitle += builder;
                                    subtitle = subtitle.Substring(0, subtitle.Length - 2);
                                }
                                //If Option C is longer than 65 for Classic Truck or Street Rod
                                else
                                {
                                    builder.Clear();
                                    for (int i = 0; i < makesByMake.Count; i++)
                                    {
                                        builder.AppendFormat("{0}, ", makesByMake[i]);
                                    }
                                    if (builder.Length + subtitle.Length - 2 <= 65)
                                    {
                                        if (subtitle.Length > 0)
                                        {
                                            subtitle += ", ";
                                        }
                                        subtitle += builder;
                                        subtitle = subtitle.Substring(0, subtitle.Length - 2);
                                    }
                                    //If Option D is longer than 65 for Classic Truck or Street Rod
                                    else
                                    {
                                        if (subtitle.Length > 0)
                                        {
                                            subtitle += ", ";
                                        }
                                        tempCount = subtitle.Length;
                                        builder.Clear();
                                        for (int i = 0; builder.Length + tempCount - 2 <= 65; i++)
                                        {
                                            builder.AppendFormat("{0}, ", makesByMake[i]);
                                            if (builder.Length + tempCount - 2 <= 65)
                                            {
                                                subtitle += makesByMake[i] + ", ";
                                            }
                                        }
                                        subtitle = subtitle.Substring(0, subtitle.Length - 2);
                                    }
                                }
                            }
                        }
                        
                    }
                    else if (!sku.Markets.Contains("Classic Truck") && !sku.Markets.Contains("Modern Muscle") && !sku.Markets.Contains("Street Rod"))
                    {
                        builder.Clear();
                        for (int i = 0; i < models.Count; i++)
                        {
                            int[] years = yearsByModel[i].ToArray();
                            int[][] result = ConsecutiveSequences(years);

                            foreach (var item in result)
                            {
                                if (item[0] != item[item.Length - 1])
                                {
                                    builder.Append(item[0].ToString().Substring(2, item[0].ToString().Length - 2) + "-" + item[item.Length - 1].ToString().Substring(2, item[item.Length - 1].ToString().Length - 2) + " " + models[i] + ", ");
                                }
                                else if (item[0] == item[item.Length - 1])
                                {
                                    builder.Append(item[item.Length - 1].ToString().Substring(2, item[item.Length - 1].ToString().Length - 2) + " " + models[i] + ", ");
                                }
                            }
                        }

                        if (builder.Length + subtitle.Length - 2 <= 65)
                        {
                            if (subtitle.Length > 0)
                            {
                                subtitle += ", ";
                            }
                            subtitle += builder;
                            subtitle = subtitle.Substring(0, subtitle.Length - 2);

                        }
                        //If Option B is longer than 65
                        else
                        {
                            
                            builder.Clear();
                            for (int i = 0; i < makesByMake.Count; i++)
                            {
                                int[] years = yearsByMake[i].ToArray();
                                int[][] result = ConsecutiveSequences(years);

                                foreach (var item in result)
                                {
                                    if (item[0] != item[item.Length - 1])
                                    {
                                        builder.Append(item[0].ToString().Substring(2, item[0].ToString().Length - 2) + "-" + item[item.Length - 1].ToString().Substring(2, item[item.Length - 1].ToString().Length - 2) + " " + makesByMake[i] + ", ");
                                    }
                                    else if (item[0] == item[item.Length - 1])
                                    {
                                        builder.Append(item[item.Length - 1].ToString().Substring(2, item[item.Length - 1].ToString().Length - 2) + " " + makesByMake[i] + ", ");
                                    }
                                }
                            }

                            if (builder.Length + subtitle.Length - 2 <= 65)
                            {
                                if (subtitle.Length > 0)
                                {
                                    subtitle += ", ";
                                }
                                subtitle += builder;
                                subtitle = subtitle.Substring(0, subtitle.Length - 2);
                            }
                            //If Option C is longer than 65
                            else
                            {
                                builder.Clear();
                                for (int i = 0; i < models.Count; i++)
                                {
                                    builder.AppendFormat("{0}, ", models[i]);
                                }

                                if (builder.Length + subtitle.Length - 2 <= 65)
                                {
                                    if (subtitle.Length > 0)
                                    {
                                        subtitle += ", ";
                                    }
                                    subtitle += builder;
                                    subtitle = subtitle.Substring(0, subtitle.Length - 2);
                                }
                                //If Option D is longer than 65
                                else
                                {
                                    builder.Clear();
                                    for (int i = 0; i < makesByMake.Count; i++)
                                    {
                                        builder.AppendFormat("{0}, ", makesByMake[i]);
                                    }

                                    if (builder.Length + subtitle.Length - 2 <= 65)
                                    {
                                        if (subtitle.Length > 0)
                                        {
                                            subtitle += ", ";
                                        }
                                        subtitle += builder;
                                        subtitle = subtitle.Substring(0, subtitle.Length - 2);
                                    }
                                    //If Option E is longer than 65
                                    else
                                    {
                                        if (subtitle.Length > 0)
                                        {
                                            subtitle += ", ";
                                        }
                                        builder.Clear();
                                        tempCount = subtitle.Length;
                                        for (int i = 0; builder.Length + tempCount - 2 <= 65; i++)
                                        {
                                            builder.AppendFormat("{0}, ", makesByMake[i]);
                                            if (builder.Length + tempCount - 2 <= 65)
                                            {
                                                subtitle += makesByMake[i] + ", ";
                                            }
                                        }
                                        subtitle = subtitle.Substring(0, subtitle.Length - 2);
                                    }
                                }
                            }
                        }
                    } else if ((sku.Markets.Contains("Classic Truck") || sku.Markets.Contains("Modern Muscle") || sku.Markets.Contains("Street Rod")) && (sku.Markets.Contains("Modern Truck") || sku.Markets.Contains("Muscle Car") || sku.Markets.Contains("Sport Compact")))
                    {
                        builder.Clear();
                        for (int i = 0; i < models.Count; i++)
                        {
                            int[] yearArray = yearsByModel[i].ToArray();
                            int[][] result = ConsecutiveSequences(yearArray);

                            foreach (var item in result)
                            {
                                if (item[0] != item[item.Length - 1])
                                {
                                    builder.Append(item[0].ToString().Substring(2, item[0].ToString().Length - 2) + "-" + item[item.Length - 1].ToString().Substring(2, item[item.Length - 1].ToString().Length - 2) + " " + models[i] + ", ");
                                }
                                else if (item[0] == item[item.Length - 1])
                                {
                                    builder.Append(item[item.Length - 1].ToString().Substring(2, item[item.Length - 1].ToString().Length - 2) + " " + models[i] + ", ");
                                }
                            }
                        }
                        if (builder.Length + subtitle.Length - 2 <= 65)
                        {
                            if (subtitle.Length > 0)
                            {
                                subtitle += ", ";
                            }
                            subtitle += builder;
                            subtitle = subtitle.Substring(0, subtitle.Length - 2);
                        }
                        else
                        {
                            builder.Clear();
                            for (int i = 0; i < models.Count; i++)
                            {
                                builder.AppendFormat("{0}, ", models[i]);
                            }

                            if (builder.Length + subtitle.Length - 2 <= 65)
                            {
                                if (subtitle.Length > 0)
                                {
                                    subtitle += ", ";
                                }
                                subtitle += builder;
                                subtitle = subtitle.Substring(0, subtitle.Length - 2);

                            }
                            else
                            {
                                if (subtitle.Length > 0)
                                {
                                    subtitle += ", ";
                                }
                                builder.Clear();
                                for (int i = 0; builder.Length - 2 <= 65; i++)
                                {
                                    builder.AppendFormat("{0}, ", models[i]);
                                    if (builder.Length + subtitle.Length - 2 <= 65)
                                    {
                                        subtitle += models[i] + ", ";
                                    }
                                }
                                subtitle = subtitle.Substring(0, subtitle.Length - 2);
                            }
                        }
                        
                    }
                }
            }
            //Console.WriteLine(subtitle.Length);
            //Console.WriteLine(subtitle);

            return subtitle;
        }

        

        //Method that groups consecutive years together
        public static int[][] ConsecutiveSequences(int[] a)
        {
            int mod;
            if (a.Length == 0)
                return new int[0][];
            else if (a.Length == 1)
            {
                return new[] { a };
            }
            var r = new gS.Collections.Generic.List<int[]>();
            int[] cur;
            int indexoffirst = 0;
            for (int i = 1; i < a.Length; i++)
            {
                if (a[i - 1] + 1 < a[i])
                {
                    cur = new int[i - indexoffirst];
                    array.Copy(a, indexoffirst, cur, 0, cur.Length);
                    r.Add(cur);
                    indexoffirst = i;
                } 
            }
            cur = new int[a.Length - indexoffirst];
            array.Copy(a, indexoffirst, cur, 0, cur.Length);
            r.Add(cur);
            return r.ToArray();
        }
    }
}
