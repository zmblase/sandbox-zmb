﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKUSubtitles
{
    public class Driveline
    {
        public List<EngineFamily> EngineFamily { get; set; }
        public List<string> TransFamily { get; set; }
        public List<string> AxleFamily { get; set; }

        public Driveline(List<EngineFamily> engfam, List<string> tranfam, List<string> axlefam)
        {
            this.EngineFamily = engfam;
            this.TransFamily = tranfam;
            this.AxleFamily = axlefam;
        }
    }
}
