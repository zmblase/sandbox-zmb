﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKUSubtitles
{
    public class EngineFamily
    {
        public string EngineFam { get; set; }
        public List<string> EngineSubFam { set; get; }

        public EngineFamily (string engFam, List<string> engSubFam)
        {
            this.EngineFam = engFam;
            this.EngineSubFam = engSubFam;
        }
    }
}
