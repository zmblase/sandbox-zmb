﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKUSubtitles
{
    public class Universal
    {
        public List<Attribute> AttList  { get; set; } 

        public Universal (List<Attribute> att)
        {
            this.AttList = att;
        }
    }
}
