﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKUSubtitles
{
    public class YMM
    {
        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }

        public YMM(int year, string make, string model)
        {
            this.Year = year;
            this.Make = make;
            this.Model = model;
        }
    }
}
