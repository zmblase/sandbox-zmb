﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKUSubtitles
{
    public class SKU
    {
        public List<string> Markets { get; set; }
        public List<string> PedalApps { get; set; }
        public List<string> RaceTypes { get; set; }
        public List<YMM> YMM { get; set; }

        public SKU(List<string> markets, List<string> pedalApps, List<string> raceTypes, List<YMM> YMM)
        {
            this.Markets = markets;
            this.PedalApps = pedalApps;
            this.RaceTypes = raceTypes;
            this.YMM = YMM;
        }
    }
}
