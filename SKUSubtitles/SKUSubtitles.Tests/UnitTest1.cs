﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SKUSubtitles;

namespace SKUSubtitles.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            YMM h1 = new YMM(1992, "Chevy", "Camaro");
            YMM h2 = new YMM(1991, "Chevy", "Camaro");
            YMM h3 = new YMM(1972, "Ford", "Nova");
            YMM h4 = new YMM(1973, "Ford", "Nova");
            YMM h5 = new YMM(1990, "Chevy", "Camaro");
            YMM h6 = new YMM(1977, "Apple", "Avalanche");
            YMM h7 = new YMM(1998, "Chevy", "Impala");
            YMM h8 = new YMM(1998, "Chevy", "Camaro");
            List<string> hMarkets = new List<string>(new string[] { "Classic Truck", "Pedal Car" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Casual", "Race", "Sport" });
            var hYMMs = new List<YMM> { h1, h2, h3, h4, h5, h6, h7, h8 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Firebird, Houston, 77 Apple, 90-92 Chevy, 98 Chevy, 72-73 Ford");
        }

        [TestMethod]
        public void PedalCarA()
        {
            YMM h1 = new YMM(1992, "Chevy", "Camaro");
            YMM h2 = new YMM(1991, "Chevy", "Camaro");
            YMM h3 = new YMM(1972, "Ford", "Nova");
            YMM h4 = new YMM(1973, "Ford", "Nova");
            YMM h5 = new YMM(1990, "Chevy", "Camaro");
            YMM h6 = new YMM(1977, "Apple", "Avalanche");
            YMM h7 = new YMM(1998, "Chevy", "Impala");
            YMM h8 = new YMM(1998, "Chevy", "Camaro");
            List<string> hMarkets = new List<string>(new string[] { "Pedal Car" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston", "Murray", "Zephyr", "Floor Mounted" });
            List<string> hRaceTypes = new List<string>(new string[] { "Casual", "Race", "Sport" });
            var hYMMs = new List<YMM> { };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Firebird, Floor Mounted, Houston, Murray, Zephyr");
        }

        [TestMethod]
        public void PedalCarB()
        {
            YMM h1 = new YMM(1992, "Chevy", "Camaro");
            YMM h2 = new YMM(1991, "Chevy", "Camaro");
            YMM h3 = new YMM(1972, "Ford", "Nova");
            YMM h4 = new YMM(1973, "Ford", "Nova");
            YMM h5 = new YMM(1990, "Chevy", "Camaro");
            YMM h6 = new YMM(1977, "Apple", "Avalanche");
            YMM h7 = new YMM(1998, "Chevy", "Impala");
            YMM h8 = new YMM(1998, "Chevy", "Camaro");
            List<string> hMarkets = new List<string>(new string[] { "Pedal Car" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston", "Murray", "Zephyr", "Floor Mounted", "Clayton", "Lockar", "Chrysler" });
            List<string> hRaceTypes = new List<string>(new string[] { "Casual", "Race", "Sport" });
            var hYMMs = new List<YMM> { };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Chrysler, Clayton, Firebird, Floor Mounted, Houston, Lockar");
        }

        [TestMethod]
        public void OpenOvalA()
        {
            YMM h1 = new YMM(1992, "Chevy", "Camaro");
            YMM h2 = new YMM(1991, "Chevy", "Camaro");
            YMM h3 = new YMM(1972, "Ford", "Nova");
            YMM h4 = new YMM(1973, "Ford", "Nova");
            YMM h5 = new YMM(1990, "Chevy", "Camaro");
            YMM h6 = new YMM(1977, "Apple", "Avalanche");
            YMM h7 = new YMM(1998, "Chevy", "Impala");
            YMM h8 = new YMM(1998, "Chevy", "Camaro");
            List<string> hMarkets = new List<string>(new string[] { "Oval Track" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h1, h2, h3, h4, h5, h6, h7, h8 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Karting, Legends, Midget, Mini Sport");
        }

        [TestMethod]
        public void OpenOvalB()
        {
            YMM h1 = new YMM(1992, "Chevy", "Camaro");
            YMM h2 = new YMM(1991, "Chevy", "Camaro");
            YMM h3 = new YMM(1972, "Ford", "Nova");
            YMM h4 = new YMM(1973, "Ford", "Nova");
            YMM h5 = new YMM(1990, "Chevy", "Camaro");
            YMM h6 = new YMM(1977, "Apple", "Avalanche");
            YMM h7 = new YMM(1998, "Chevy", "Impala");
            YMM h8 = new YMM(1998, "Chevy", "Camaro");
            List<string> hMarkets = new List<string>(new string[] { "Oval Track" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport", "Dirt Late Model", "Hobby Stock", "Pony Stock" });
            var hYMMs = new List<YMM> { h1, h2, h3, h4, h5, h6, h7, h8 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Dirt Late Model, Hobby Stock, Karting, Legends, Midget");
        }

        [TestMethod]
        public void TBucketA()
        {
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            List<string> hMarkets = new List<string>(new string[] { "T-Bucket" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h1, h2, h3, h4, h5, h8 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "90-92 Caprice, 98 Caprice, 72-73 Cougar");
        }

        [TestMethod]
        public void TBucketB()
        {
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            List<string> hMarkets = new List<string>(new string[] { "T-Bucket" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h1, h2, h3, h4, h5, h7, h8, h9, h10, h11 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Aspen, Caprice, Corvette, Cougar, Seville");
        }

        [TestMethod]
        public void TBucketC()
        {
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            List<string> hMarkets = new List<string>(new string[] { "T-Bucket" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h1, h2, h3, h4, h5, h6, h7, h8, h9, h10, h11, h12, h13 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Aspen, Caprice, Challenger, Civic, Corvette, Cougar, Element");
        }

        [TestMethod]
        public void ClassicTruckA()
        {
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            List<string> hMarkets = new List<string>(new string[] { "Classic Truck" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h1, h2, h5, h8, h13 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "90-92 Chevy Caprice, 98 Chevy Caprice, 04 Honda Civic");
        }

        [TestMethod]
        public void ClassicTruckB()
        {
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            List<string> hMarkets = new List<string>(new string[] { "Classic Truck" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h1, h2, h5, h6, h8, h10, h11, };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "90-92 Chevy, 98 Chevy, 70-71 Dodge, 77 Dodge");
        }

        [TestMethod]
        public void ClassicTruckC()
        {
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            List<string> hMarkets = new List<string>(new string[] { "Classic Truck" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h1, h2, h3, h4, h5, h8, h10, h11, h14, h15 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Chevy Caprice, Dodge Aspen, Ford Cougar");
        }

        [TestMethod]
        public void ClassicTruckD()
        {
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            List<string> hMarkets = new List<string>(new string[] { "Classic Truck" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h1, h2, h3, h4, h5, h6, h7, h8, h10, h11, h12, h14, h15 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Chevy, Dodge, Ford, Honda");
        }

        [TestMethod]
        public void ClassicTruckE()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Classic Truck" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h1, h2, h3, h4, h5, h6, h7, h8, h9, h10, h11, h12, h14, h15, h16, h17, h18 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Cadillac, Chevy, Dodge, Ford, Freightliner, GMC, Honda");
        }

        [TestMethod]
        public void ModernMuscleA()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Classic Truck" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h13, h14, h15, h3, h4 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "88 Dodge Aspen, 90 Dodge Aspen, 72-73 Ford Cougar, 04 Honda Civic");
        }

        [TestMethod]
        public void ModernMuscleB()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Modern Muscle" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h13, h14, h15, h3, h4, h9 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "88 Aspen, 90 Aspen, 04 Civic, 72-73 Cougar, 68 Seville");
        }

        [TestMethod]
        public void ModernMuscleC()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Modern Muscle" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h13, h14, h15, h3, h4, h9, h18, h12 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Aspen, Civic, Cougar, Element, Jimmy, Seville");
        }

        [TestMethod]
        public void ModernMuscleD()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Modern Muscle" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h13, h14, h15, h3, h4, h9, h18, h12, h6, h1, h7 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Cadillac, Chevy, Dodge, Ford, GMC, Honda");
        }

        [TestMethod]
        public void ModernMuscleE()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Modern Muscle" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h13, h14, h15, h3, h4, h9, h18, h12, h6, h1, h7, h17, h16 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Cadillac, Chevy, Dodge, Ford, Freightliner, GMC, Honda");
        }

        [TestMethod]
        public void ModernTruckA()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Modern Truck" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h10, h11, h18 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "91-92 Chevy Caprice, 70-71 Dodge Aspen, 55 GMC Jimmy");
        }

        [TestMethod]
        public void ModernTruckB()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Modern Truck" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h10, h11, h18, h17 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "70-71 Aspen, 91-92 Caprice, 79 Coronado, 55 Jimmy");
        }

        [TestMethod]
        public void ModernTruckC()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Modern Truck" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h10, h11, h18, h6, h14, h4 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "91-92 Chevy, 70-71 Dodge, 77 Dodge, 88 Dodge, 73 Ford, 55 GMC");
        }

        [TestMethod]
        public void ModernTruckD()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Modern Truck" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h10, h11, h18, h6, h14, h4, h7 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Aspen, Caprice, Challenger, Corvette, Cougar, Jimmy");
        }

        [TestMethod]
        public void ModernTruckE()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Modern Truck" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h10, h11, h18, h6, h14, h4, h7, h9, h12 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Cadillac, Chevy, Dodge, Ford, GMC, Honda");
        }

        [TestMethod]
        public void ModernTruckF()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Modern Truck" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h10, h11, h18, h6, h14, h4, h7, h9, h12, h16, h17 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Cadillac, Chevy, Dodge, Ford, Freightliner, GMC, Honda");
        }

        [TestMethod]
        public void MuscleCarA()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Muscle Car" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h3, h4, h6 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "91-92 Chevy Caprice, 77 Dodge Challenger, 72-73 Ford Cougar");
        }

        [TestMethod]
        public void MuscleCarB()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Muscle Car" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h3, h4, h6, h18 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "91-92 Caprice, 77 Challenger, 72-73 Cougar, 55 Jimmy");
        }

        [TestMethod]
        public void MuscleCarC()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Muscle Car" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h3, h4, h6, h18, h8, h14 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "91-92 Chevy, 98 Chevy, 77 Dodge, 88 Dodge, 72-73 Ford, 55 GMC");
        }

        [TestMethod]
        public void MuscleCarD()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Muscle Car" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h3, h4, h18, h8, h14, h9, h13 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Aspen, Caprice, Civic, Cougar, Jimmy, Seville");
        }

        [TestMethod]
        public void MuscleCarE()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Muscle Car" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h3, h4, h18, h8, h14, h9, h13, h6, h7 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Cadillac, Chevy, Dodge, Ford, GMC, Honda");
        }

        [TestMethod]
        public void MuscleCarF()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Muscle Car" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h3, h4, h18, h8, h14, h9, h13, h6, h7, h16, h17 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Cadillac, Chevy, Dodge, Ford, Freightliner, GMC, Honda");
        }

        [TestMethod]
        public void SportCompactA()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Sport Compact" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h3, h4, h6 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "91-92 Chevy Caprice, 77 Dodge Challenger, 72-73 Ford Cougar");
        }

        [TestMethod]
        public void SportCompactB()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Sport Compact" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h3, h4, h6, h18 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "91-92 Caprice, 77 Challenger, 72-73 Cougar, 55 Jimmy");
        }

        [TestMethod]
        public void SportCompactC()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Sport Compact" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h3, h4, h6, h18, h8, h14 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "91-92 Chevy, 98 Chevy, 77 Dodge, 88 Dodge, 72-73 Ford, 55 GMC");
        }

        [TestMethod]
        public void SportCompactD()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Sport Compact" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h3, h4, h18, h8, h14, h9, h13 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Aspen, Caprice, Civic, Cougar, Jimmy, Seville");
        }

        [TestMethod]
        public void SportCompactE()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Sport Compact" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h3, h4, h18, h8, h14, h9, h13, h6, h7 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Cadillac, Chevy, Dodge, Ford, GMC, Honda");
        }

        [TestMethod]
        public void SportCompactF()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Sport Compact" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h3, h4, h18, h8, h14, h9, h13, h6, h7, h16, h17 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Cadillac, Chevy, Dodge, Ford, Freightliner, GMC, Honda");
        }

        [TestMethod]
        public void StreetRodA()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1989, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Street Rod" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h3, h4, h14, h15 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "91-92 Chevy Caprice, 88-89 Dodge Aspen, 72-73 Ford Cougar");
        }

        [TestMethod]
        public void StreetRodB()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1989, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Street Rod" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h3, h4, h14, h15, h7 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "91-92 Chevy, 98 Chevy, 88-89 Dodge, 72-73 Ford");
        }

        [TestMethod]
        public void StreetRodC()
        {
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1990, "Dodge", "Aspen");
            List<string> hMarkets = new List<string>(new string[] { "Street Rod" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h1, h2, h3, h4, h5, h8, h10, h11, h14, h15 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Chevy Caprice, Dodge Aspen, Ford Cougar");
        }

        [TestMethod]
        public void StreetRodD()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1989, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Street Rod" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h3, h4, h14, h15, h7, h10, h11, h15, h18 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Chevy, Dodge, Ford, GMC");
        }

        [TestMethod]
        public void StreetRodF()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1989, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Street Rod" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h3, h4, h14, h15, h7, h10, h11, h15, h18, h9, h17, h16, h12 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Cadillac, Chevy, Dodge, Ford, Freightliner, GMC, Honda");
        }

        [TestMethod]
        public void OverlapA()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1989, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Street Rod", "Muscle Car" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h3, h4 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "91-92 Chevy Caprice, 72-73 Ford Cougar");
        }

        [TestMethod]
        public void OverlapB()
        {
            YMM h16 = new YMM(2001, "International", "Durastar");
            YMM h1 = new YMM(1992, "Chevy", "Caprice");
            YMM h2 = new YMM(1991, "Chevy", "Caprice");
            YMM h3 = new YMM(1972, "Ford", "Cougar");
            YMM h4 = new YMM(1973, "Ford", "Cougar");
            YMM h5 = new YMM(1990, "Chevy", "Caprice");
            YMM h6 = new YMM(1977, "Dodge", "Challenger");
            YMM h7 = new YMM(1998, "Chevy", "Corvette");
            YMM h8 = new YMM(1998, "Chevy", "Caprice");
            YMM h9 = new YMM(1968, "Cadillac", "Seville");
            YMM h10 = new YMM(1970, "Dodge", "Aspen");
            YMM h11 = new YMM(1971, "Dodge", "Aspen");
            YMM h12 = new YMM(2003, "Honda", "Element");
            YMM h13 = new YMM(2004, "Honda", "Civic");
            YMM h14 = new YMM(1988, "Dodge", "Aspen");
            YMM h15 = new YMM(1989, "Dodge", "Aspen");
            YMM h17 = new YMM(1979, "Freightliner", "Coronado");
            YMM h18 = new YMM(1955, "GMC", "Jimmy");

            List<string> hMarkets = new List<string>(new string[] { "Street Rod", "Muscle Car" });
            List<string> hPedalApps = new List<string>(new string[] { "Firebird", "Houston" });
            List<string> hRaceTypes = new List<string>(new string[] { "Karting", "Legends", "Midget", "Mini Sport" });
            var hYMMs = new List<YMM> { h2, h1, h3, h4, h7, h14, h12, h13 };

            SKU h1313 = new SKU(hMarkets, hPedalApps, hRaceTypes, hYMMs);

            string subTitle = Program.VehicleSubtitle(h1313);

            Assert.AreEqual(subTitle, "Aspen, Caprice, Civic, Corvette, Cougar, Element");
        }

        [TestMethod]
        public void EngineA()
        {
            var engFam = new List<EngineFamily>();
            List<string> sub1 = new List<string>(new string[] { "Air Intake" });
            EngineFamily eng1 = new EngineFamily("Stock", sub1);
            engFam.Add(eng1);
            List<string> sub2 = new List<string>(new string[] { "Plug" , "Testing"});
            EngineFamily eng2 = new EngineFamily("Block", sub2);
            engFam.Add(eng2);
            List<string> sub3 = new List<string>(new string[] { "Cam" });
            EngineFamily eng3 = new EngineFamily("Camshaft", sub3);
            engFam.Add(eng3);

            List<string> tranFam = new List<string>(new string[] { });
            List<string> axleFam = new List<string>(new string[] { });
            Driveline driveline = new Driveline(engFam, tranFam, axleFam);

            string subTitle = Program.DrivelineSubtitle(driveline);

            Assert.AreEqual(subTitle, "Block Plug Testing, Camshaft Cam, Stock Air Intake");
        }

        [TestMethod]
        public void EngineB()
        {
            var engFam = new List<EngineFamily>();
            List<string> sub1 = new List<string>(new string[] { "Air Intake", "Distributor", "Cam" });
            EngineFamily eng1 = new EngineFamily("Stock", sub1);
            engFam.Add(eng1);
            List<string> sub2 = new List<string>(new string[] { "Performance", "Pro", "Freeze", "Plug" });
            EngineFamily eng2 = new EngineFamily("Block", sub2);
            engFam.Add(eng2);
            List<string> sub3 = new List<string>(new string[] { "Beam", "Cam", "Piston", "Hardened", "Crankshaft" });
            EngineFamily eng3 = new EngineFamily("Camshaft", sub3);
            engFam.Add(eng3);

            List<string> tranFam = new List<string>(new string[] { });
            List<string> axleFam = new List<string>(new string[] { });
            Driveline driveline = new Driveline(engFam, tranFam, axleFam);

            string subTitle = Program.DrivelineSubtitle(driveline);

            Assert.AreEqual(subTitle, "Block, Camshaft, Stock");
        }
        
        [TestMethod]
        public void EngineC()
        {
            var engFam = new List<EngineFamily>();
            List<string> sub1 = new List<string>(new string[] { "Air Intake", "Distributor", "Cam" });
            EngineFamily eng1 = new EngineFamily("Stock", sub1);
            engFam.Add(eng1);
            List<string> sub2 = new List<string>(new string[] { "Performance", "Pro", "Freeze", "Plug" });
            EngineFamily eng2 = new EngineFamily("Block", sub2);
            engFam.Add(eng2);
            List<string> sub3 = new List<string>(new string[] { "Beam", "Cam", "Piston", "Hardened", "Crankshaft" });
            EngineFamily eng3 = new EngineFamily("Camshaft", sub3);
            engFam.Add(eng3);
            List<string> sub4 = new List<string>(new string[] { });
            EngineFamily eng4 = new EngineFamily("testingtestingtestingtestingtestingtestingtesting", sub3);
            engFam.Add(eng4);

            List<string> tranFam = new List<string>(new string[] { });
            List<string> axleFam = new List<string>(new string[] { });
            Driveline driveline = new Driveline(engFam, tranFam, axleFam);

            string subTitle = Program.DrivelineSubtitle(driveline);

            Assert.AreEqual(subTitle, "Block, Camshaft, Stock");
        }
        
        [TestMethod]
        public void TransmissionA()
        {
            var engFam = new List<EngineFamily>();
            List<string> tranFam = new List<string>(new string[] { "Henchcraft", "Stanley", "AFCO", "Rear", "Front" });
            List<string> axleFam = new List<string>(new string[] { });
            Driveline driveline = new Driveline(engFam, tranFam, axleFam);

            string subTitle = Program.DrivelineSubtitle(driveline);

            Assert.AreEqual(subTitle, "AFCO, Front, Henchcraft, Rear, Stanley");
        }
        
        [TestMethod]
        public void TransmissionB()
        {
            var engFam = new List<EngineFamily>();
            List<string> tranFam = new List<string>(new string[] { "Henchcraft", "Stanley", "AFCO", "Rear", "Front", "Aluminum", "Flexible", "Chrome" });
            List<string> axleFam = new List<string>(new string[] { });
            Driveline driveline = new Driveline(engFam, tranFam, axleFam);

            string subTitle = Program.DrivelineSubtitle(driveline);

            Assert.AreEqual(subTitle, "AFCO, Aluminum, Chrome, Flexible, Front, Henchcraft, Rear");
        }

        [TestMethod]
        public void AxleA()
        {
            var engFam = new List<EngineFamily>();
            List<string> tranFam = new List<string>(new string[] { });
            List<string> axleFam = new List<string>(new string[] { "Henchcraft", "Stanley", "AFCO", "Rear", "Front" });
            Driveline driveline = new Driveline(engFam, tranFam, axleFam);

            string subTitle = Program.DrivelineSubtitle(driveline);

            Assert.AreEqual(subTitle, "AFCO, Front, Henchcraft, Rear, Stanley");
        }

        [TestMethod]
        public void AxleB()
        {
            var engFam = new List<EngineFamily>();
            List<string> tranFam = new List<string>(new string[] { });
            List<string> axleFam = new List<string>(new string[] { "Henchcraft", "Stanley", "AFCO", "Rear", "Front", "Aluminum", "Flexible", "Chrome" });
            Driveline driveline = new Driveline(engFam, tranFam, axleFam);

            string subTitle = Program.DrivelineSubtitle(driveline);

            Assert.AreEqual(subTitle, "AFCO, Aluminum, Chrome, Flexible, Front, Henchcraft, Rear");
        }

        [TestMethod]
        public void UnivA()
        {
            Attribute att1 = new Attribute("1", "Diameter", "3/4'");
            Attribute att2 = new Attribute("2", "lbs", "10");
            Attribute att3 = new Attribute("3", "Color", "Orange");
            Attribute att4 = new Attribute("4", "Height", "1'");
            var Attributes = new List<Attribute> { att1, att2, att3, att4 };
            Universal univ = new Universal(Attributes);

            string subTitle = Program.UniversalSubtitle(univ);

            Assert.AreEqual(subTitle, "3/4' Diameter, 10 lbs, Orange, 1' Height");
        }

        [TestMethod]
        public void UnivB()
        {
            Attribute att1 = new Attribute("1", "Diameter", "3/4'");
            Attribute att2 = new Attribute("2", "lbs", "10");
            Attribute att3 = new Attribute("3", "Color", "Orange");
            Attribute att4 = new Attribute("4", "Height", "1'");
            Attribute att5 = new Attribute("5", "Quantity", "10");
            Attribute att6 = new Attribute("8", "Part #", "4857");
            Attribute att7 = new Attribute("6", "Origin", "United States");
            var Attributes = new List<Attribute> { att1, att2, att3, att4, att5, att6, att7 };
            Universal univ = new Universal(Attributes);

            string subTitle = Program.UniversalSubtitle(univ);

            Assert.AreEqual(subTitle, "3/4' Diameter, 10 lbs, Orange, 1' Height, 10 Quantity");
        }
    }
}